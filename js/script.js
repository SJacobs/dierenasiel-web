function showNaamRequired() {
	$('#naam_error').removeClass('hidden');
}

function hideNaamRequired() {
	$('#naam_error').addClass('hidden');
}

function showRegNrRequired() {
	$('#regnr_zerostart_error').removeClass('hidden');
}

function hideRegNrRequired() {
	$('#regnr_zerostart_error').addClass('hidden');
}

function showLeeftijdRequired() {
	$('#leeftijd_neg_error').removeClass('hidden');
}

function hideLeeftijdRequired() {
	$('#leeftijd_neg_error').addClass('hidden');
}

function OnSubmit(event) {
	handleAddAnimalButton();
}

function handleAddAnimalButton() {
	var animal = {
		'naam':     	$('#naam_input').val(),
		'leeftijd':     $('#leeftijd_input').val(),
		'regnr':    	$('#regnr_input').val(),
		'gereserveerd': $('#gereserveerd_input').is(':checked'),
		'diersoort':  	$('input[name=diersoort]:checked').val()
	};
	
	if (animal.naam.length == 0) {
		event.preventDefault();
		showNaamRequired();
	}

	if (animal.leeftijd.length > 0) {
		if (animal.leeftijd < 0) {
			event.preventDefault();
			showLeeftijdRequired();
		}
	}
	
	if (animal.regnr.length > 0) {
		if (animal.regnr.substring(0, 1) != 0) {
			event.preventDefault();
			showRegNrRequired();
		}
	}
	if (animal.diersoort == 'hond') {
		var hond = JSON.stringify(animal)
		addToStorageList('honden', hond);
	} else if (animal.diersoort == 'kat') {
		var kat = JSON.stringify(animal)
		addToStorageList('katten', kat);
	} else {
		event.preventDefault();
		alert("Geen diersoort geselecteerd!");
	}
}

function addToStorageList(listName, itemToAdd) {
	var list = JSON.parse(localStorage.getItem(listName));	
	if (!Array.isArray(list)) {
		list = [];
	}
	list.push(itemToAdd);
	localStorage.setItem(listName, JSON.stringify(list));	
}

function loadTableFromStorageList(listName, tableID) {
	var list = JSON.parse(localStorage.getItem(listName));
	if (!Array.isArray(list)) {
		list = [];
	}
	if (Array.isArray(list)) {
		for (var i = 0; i < list.length; i++) {
			var dier = JSON.parse(list[i]);
			$(tableID).append('<tr><td>'+dier.naam+'</td><td>'+dier.leeftijd+'</td><td>'+dier.regnr+'</td><td>'+dier.gereserveerd+'</td></tr>');
		}
	}	
}

function loadHondList() {
	loadTableFromStorageList('honden', '#tbl_aanwezige_honden');
}

function loadKatList() {
	loadTableFromStorageList('katten', '#tbl_aanwezige_katten');
}

function OnReady() {
	//localStorage.clear();
	loadHondList();
	loadKatList();
}

function ClearStorageList(listName) {
	localStorage.setItem(listName, JSON.stringify([]));
}

function ClearHondenTable() {
	ClearStorageList('honden');
	location.reload();
}

function ClearKattenTable() {
	ClearStorageList('katten');
	location.reload();
}

$('#btn_clear_honden_table').click(ClearHondenTable);

$('#btn_clear_katten_table').click(ClearKattenTable);

$('#naam_input').focus(showNaamRequired);

$('#naam_input').blur(hideNaamRequired);

$('#btn_diertoevoegen').click(OnSubmit);

$(document).ready(OnReady);
